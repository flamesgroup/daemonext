/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.daemonext;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

public class DaemonExtTest {

  @Mocked("read()")
  InputStream in;
  @Mocked("write(int)")
  OutputStream out;
  @Mocked("write(int)")
  OutputStream err;

  private DaemonExt daemonExt;

  @Before
  public void init() {
    daemonExt = new DaemonExt(in, new PrintStream(out), new PrintStream(err)) {
      @Override
      public List<IDaemonExtStatus> getStatuses() {
        return null;
      }
    };
  }

  @Test
  public void executeStartStopWithWriteToOutAndErrStream() throws Exception {
    new Expectations() {{
      in.read();
      result = 0;
    }};

    daemonExt.main("test", new String[0]);

    new Verifications() {{
      out.write(anyInt);
      minTimes = 1; // regular message about press any key
      err.write(anyInt);
      minTimes = 1; // WARN message about undefined system property
    }};
  }

  @Test
  public void executeStartStopWithWriteToOutStream() throws Exception {
    new Expectations() {{
      in.read();
      result = 0;
    }};

    System.setProperty("daemonExtPort", "65000");
    try {
      daemonExt.main("test", new String[0]);
    } finally {
      System.clearProperty("daemonExtPort");
    }

    new Verifications() {{
      out.write(anyInt);
      minTimes = 1; // regular message about press any key
      err.write(anyInt);
      minTimes = 0;
    }};
  }

}
