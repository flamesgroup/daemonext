/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.daemonext;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import mockit.Mocked;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

public class DaemonExtStatusTest {

  @Mocked("read()")
  InputStream in;
  @Mocked("write(int)")
  OutputStream out;
  @Mocked("write(int)")
  OutputStream err;

  private TestDaemonExt daemonExt;

  @Before
  public void initAndStart() throws Exception {
    System.setProperty("daemonExtRequestTimeout", "1000");

    daemonExt = new TestDaemonExt(in, new PrintStream(out), new PrintStream(err));

    daemonExt.init(new String[0]);
    daemonExt.start();
  }

  @After
  public void stopAndDestroy() throws Exception {
    daemonExt.stop();
    daemonExt.destroy();

    daemonExt = null;

    System.clearProperty("daemonExtRequestTimeout");
  }

  @Test
  public void callTestStatus() throws Exception {
    try (Socket clientSocket = new Socket("localhost", daemonExt.getDaemonExtEngineLocalPort())) {
      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
      BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

      bw.append("testStatusName");
      bw.newLine();
      bw.flush();

      assertEquals("testStatusResult", br.readLine());
    }
  }

  @Test
  public void callUnknownStatus() throws Exception {
    try (Socket clientSocket = new Socket("localhost", daemonExt.getDaemonExtEngineLocalPort())) {
      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
      BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

      bw.append("unknown");
      bw.newLine();
      bw.flush();

      assertThat(br.readLine(), allOf(startsWith("ERROR"), containsString("isn't found")));
    }
  }

  @Test
  public void timeoutStatus() throws Exception {
    try (Socket clientSocket = new Socket("localhost", daemonExt.getDaemonExtEngineLocalPort())) {
      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
      BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

      assertThat(br.readLine(), allOf(startsWith("ERROR"), containsString("timeout")));
    }
  }

  private class TestDaemonExt extends DaemonExt {

    public TestDaemonExt(final InputStream in, final PrintStream out, final PrintStream err) {
      super(in, out, err);
    }

    @Override
    public List<IDaemonExtStatus> getStatuses() {
      return Arrays.asList(new IDaemonExtStatus() {
        @Override
        public String getName() {
          return "testStatusName";
        }

        @Override
        public String[] getDescription() {
          return new String[] {"testStatusDescription"};
        }

        @Override
        public String execute(final String[] args) {
          return "testStatusResult";
        }
      });
    }

    public int getDaemonExtEngineLocalPort() {
      return getDaemonExtEngine().getLocalPort();
    }
  }

}
