/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.daemonext;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

public abstract class DaemonExt {

  public static final int EXIT_CODE_TO_BE_RESTARTED = 123;

  private static final String PORT_PROPERTY_NAME = "daemonExtPort";
  private static final int PORT_AUTOMATICALLY_ALLOCATED = 0;
  private static final String REQUEST_TIMEOUT_PROPERTY_NAME = "daemonExtRequestTimeout";
  private static final int REQUEST_TIMEOUT_DEFAULT = 10_000;

  private DaemonExtEngine daemonExtEngine;

  private final InputStream in;
  private final PrintStream out;
  private final PrintStream err;


  public DaemonExt(final InputStream in, final PrintStream out, final PrintStream err) {
    this.in = in;
    this.out = out;
    this.err = err;
  }

  public DaemonExt() {
    this(System.in, System.out, System.err);
  }

  protected InputStream getIn() {
    return in;
  }

  protected PrintStream getOut() {
    return out;
  }

  protected PrintStream getErr() {
    return err;
  }

  protected DaemonExtEngine getDaemonExtEngine() {
    return daemonExtEngine;
  }

  public abstract List<IDaemonExtStatus> getStatuses();

  public void init(String[] args) throws Exception {
  }

  public void start() throws Exception {
    Integer port = Integer.getInteger(PORT_PROPERTY_NAME);
    if (port == null) {
      warn("system property [%s] isn't specified or incorrect - value [%d] will be used to port number automatically allocation", PORT_PROPERTY_NAME, PORT_AUTOMATICALLY_ALLOCATED);
      port = PORT_AUTOMATICALLY_ALLOCATED;
    }

    Integer requestTimeout = Integer.getInteger(REQUEST_TIMEOUT_PROPERTY_NAME, REQUEST_TIMEOUT_DEFAULT);

    daemonExtEngine = new DaemonExtEngine(port, requestTimeout);
    daemonExtEngine.start();
  }

  public void stop() throws Exception {
    if (daemonExtEngine != null) {
      daemonExtEngine.stop();
    }
  }

  public void destroy() throws Exception {
    daemonExtEngine = null;
  }

  public final void main(String appName, String[] args) throws Exception {
    init(args);
    start();
    getOut().println("Press Enter to exit ...");
    getIn().read();
    stop();
    destroy();
  }

  private void print(PrintStream ps, String level, String format, Object... args) {
    ps.print(level);
    ps.print(": ");
    ps.printf(format, args);
    ps.println();
  }

  protected void warn(String format, Object... args) {
    print(getErr(), "WARN", format, args);
  }

  protected void error(String format, Object... args) {
    print(getErr(), "ERROR", format, args);
  }

  protected void error(Exception e) {
    print(getErr(), "ERROR", e.getMessage());
    e.printStackTrace(getErr());
  }

  protected class DaemonExtEngine implements Runnable {

    private final int port;
    private final int requestTimeout;

    private final ServerSocket serverSocket;

    public DaemonExtEngine(final int port, final int requestTimeout) throws IOException {
      this.port = port;
      this.requestTimeout = requestTimeout;

      serverSocket = new ServerSocket();
    }

    public int getLocalPort() {
      return serverSocket.getLocalPort();
    }

    private void start() throws IOException {
      serverSocket.bind(new InetSocketAddress("localhost", port));
      new Thread(this, this.getClass().getSimpleName()).start();
    }

    private void stop() throws IOException {
      serverSocket.close();
    }

    @Override
    public void run() {
      while (true) {
        try {
          try (Socket clientSocket = serverSocket.accept()) {
            clientSocket.setSoTimeout(requestTimeout);

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            String statusLine = null;
            try {
              statusLine = br.readLine();
              if (statusLine != null) {
                statusLine = statusLine.trim();
              } else {
                statusLine = ""; // no any data entering, so empty
              }
            } catch (SocketTimeoutException e) {
              // this is expected behaviour in case of long status request entering
            } catch (IOException e) {
              error(e);
              continue;
            }

            bw.append(execute(statusLine));
            bw.newLine();
            bw.flush();
          }
        } catch (SocketException e) {
          break;
        } catch (IOException e) {
          error(e);
          break;
        }
      }
    }

    private String execute(final String statusLine) {
      if (statusLine == null) {
        return formatErrorResult("status request entering " + requestTimeout + "ms timeout");
      } else if (statusLine.isEmpty()) {
        return formatErrorResult("status request is empty");
      } else if (statusLine.equals("*")) {
        return formatAllAvailableStatuses();
      } else {
        String[] statusLineSplit = statusLine.split("\\s+");

        String statusName = statusLineSplit[0];
        String[] statusArgs = Arrays.copyOfRange(statusLineSplit, 1, statusLineSplit.length);

        List<IDaemonExtStatus> daemonExtStatuses = getStatuses();
        if (daemonExtStatuses == null) {
          return formatErrorResult("statuses isn't defined");
        } else {
          for (IDaemonExtStatus daemonExtStatus : daemonExtStatuses) {
            if (statusName.equals(daemonExtStatus.getName())) {
              return daemonExtStatus.execute(statusArgs);
            }
          }
          return formatErrorResult("status isn't found");
        }
      }
    }

    private String formatErrorResult(final String result) {
      return "ERROR: " + result;
    }

    private String formatAllAvailableStatuses() {
      List<IDaemonExtStatus> daemonExtStatuses = getStatuses();
      if (daemonExtStatuses == null) {
        return "Statuses isn't defined";
      } else if (daemonExtStatuses.isEmpty()) {
        return "Statuses is empty";
      } else {
        StringBuilder sb = new StringBuilder();
        sb.append("Available statuses:");
        sb.append(System.lineSeparator());
        for (IDaemonExtStatus daemonExtStatus : daemonExtStatuses) {
          String indentedName = "  " + daemonExtStatus.getName() + " - ";
          String indent = createIndentLine(indentedName.length());

          sb.append(indentedName);

          String[] description = daemonExtStatus.getDescription();
          if (description != null && description.length > 0) {

            for (int i = 0; i < description.length; i++) {
              if (i != 0) {
                sb.append(indent);
              }
              sb.append(description[i]);
              if (i != (description.length - 1)) {
                sb.append(System.lineSeparator());
              }
            }
          }
          sb.append(System.lineSeparator());
        }
        return sb.toString();
      }
    }

    private String createIndentLine(int length) {
      StringBuilder sb = new StringBuilder();
      while (length-- > 0) {
        sb.append(' ');
      }
      return sb.toString();
    }
  }

}
