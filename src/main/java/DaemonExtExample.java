/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.flamesgroup.daemonext.DaemonExt;
import com.flamesgroup.daemonext.IDaemonExtStatus;

import java.util.Collections;
import java.util.List;

public class DaemonExtExample extends DaemonExt {

  @Override
  public void start() throws Exception {

    // application start logic

    super.start();
  }

  @Override
  public void stop() throws Exception {
    super.stop();

    // application stop logic
  }

  @Override
  public List<IDaemonExtStatus> getStatuses() {
    return Collections.singletonList(new IDaemonExtStatus() {
      @Override
      public String getName() {
        return "currentTimeMillis";
      }

      @Override
      public String[] getDescription() {
        return new String[] {
            "show the current time in milliseconds"
        };
      }

      @Override
      public String execute(final String[] args) {
        return String.valueOf(System.currentTimeMillis());
      }
    });
  }

  public static void main(String[] args) throws Exception {
    new DaemonExtExample().main("DaemonExtExample", args);
  }

}
