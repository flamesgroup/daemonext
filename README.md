# daemonext

Extension of [Apache Commons Daemon](https://commons.apache.org/proper/commons-daemon/index.html) library with functionality to execute specified status commands on running process via socket.

## Overview

[Apache Commons Daemon](https://commons.apache.org/proper/commons-daemon/index.html) library provides a portable functionality of starting and stopping a Java Virtual Machine (JVM) that is running server applications (daemon). But it does not provide any functionality to execute some status commands on already running applications.

**daemonext** resolves this requirement by starting additional thread which listens on socket bound to predefined port of localhost interface and processes all incoming connection with request and response of specified status commands in text format.

**daemonext** can be configured with next system properties:
* `daemonExtPort` - socket bind port
* `daemonExtRequestTimeout` - socket read timeout in milliseconds

**daemonext** introduce one special status command `*` which output all available statuses with its descriptions.

## Structure

**daemonext** uses standard [Maven](https://maven.apache.org) project layout.

## Build

**daemonext** could be compiled, installed and deployed with regular [Maven commands](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).

## Usage examples

```java
import com.flamesgroup.daemonext.DaemonExt;
import com.flamesgroup.daemonext.IDaemonExtStatus;

import java.util.Collections;
import java.util.List;

public class DaemonExtExample extends DaemonExt {

  @Override
  public void start() throws Exception {
    // application start logic

    super.start();
  }

  @Override
  public void stop() throws Exception {
    super.stop();

    // application stop logic
  }

  @Override
  public List<IDaemonExtStatus> getStatuses() {
    return Collections.singletonList(new IDaemonExtStatus() {
      @Override
      public String getName() {
        return "currentTimeMillis";
      }

      @Override
      public String[] getDescription() {
        return new String[] {
            "show the current time in milliseconds"
        };
      }

      @Override
      public String execute(final String[] args) {
        return String.valueOf(System.currentTimeMillis());
      }
    });
  }

}
```

If for some reason (testing purpose) server application must be run as command line application then helper `main` method can be used.

```java
public class DaemonExtExample extends DaemonExt {
  // ...
  public static void main(String[] args) throws Exception {
    new DaemonExtExample().main("DaemonExtExample", args);
  }
  // ...
}
```

## Questions

If you have any questions about **daemonext**, feel free to create issue with it.
